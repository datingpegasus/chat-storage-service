/**
 * Main application file
 */
import * as express from 'express'
import * as http from 'http'
import * as mongoose from 'mongoose'

import config from './config/environment'
import setup from './src';
import expressConfig from './config/express'
import routesConfig from './routes'

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
// Setup server
const app: express.Application = express();
const server = http.createServer(app)
expressConfig(app)
routesConfig(app)
setup().then(() => {
  // Start server
  server.listen(config.port, config.ip, () => {
    console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
  })

}).catch((error) => {
  console.log('faild to load a server ', error)
})

// Expose app
export default app
