import message from './src/api/message'
import * as express from 'express'

const routes = (app: express.Application): void => {
    // Insert routes below
    app.use('/api/message', message);

    // All other routes should 404
    app.route('/*')
        .get((req: express.Request, res: express.Response) => {
            res.sendStatus(404)
        });
};

export default routes
