export interface IUser {
    id: string,
    email: string,
    firstName: string,
    lastName: string,
    verified: boolean,
    createdAt: Date,
    updatedAt: Date
}
