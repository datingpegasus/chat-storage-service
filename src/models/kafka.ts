import {IMessage} from './message';
import * as kafka from 'kafkajs'
export interface IKafkaProducer {
    pushToSavedMessage(message: IMessage): Promise<boolean>
    pushToDeliveredMessageSaved(message: IMessage): Promise<boolean>
    pushUserConversations(conversations: any): Promise<boolean>
    pushUserConversationMessages(conversationMessages: IMessage[], userId: string): Promise<boolean>;
    pushToKafkaTopic(data: any, topic: string): Promise<boolean>
}
export enum KafkaProducerTopics {
    savedMessages = 'SavedMessage',
    deliveredMessagesSaved = 'DeliveredMessageSaved',
    userConversations = 'UserConversations',
    userConversationMessages = 'UserConversationMessages'
}

export interface IKafkaConsumer {
    processKafkaTopic(topic: string, kafkaMessage: kafka.KafkaMessage, partition: number): Promise<void>;
    subscribeToTopics(): Promise<void>;
    commitOffset(topicName: string, partition: number, offset: string): Promise<boolean>;
}

export enum KafkaSubscribeTopics {
    sentMessages = 'ProfanityCheckComplete',
    messageDelivered = 'MessageDelivered',
    userConnectedToSocket = 'UserConnectedToSocket',
    userAuthenticated = 'UserAuthenticated',
    userConnectedToConversation = 'UserConnectedToConversation'
}
