import {IUser} from './user';
import {IMessage} from './message';

export interface IChatStorageService {
    sentMessage(message: IMessage): Promise<boolean>;
    deliveredMessage(message: IMessage): Promise<boolean>;
    userConnectedToSocket(userId: string): Promise<boolean>;
    userAuthenticated(user: IUser): Promise<boolean>;
    userConnectedToConversation(conversation: object): Promise<boolean>;
}
