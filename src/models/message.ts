import {IUser} from './user';
import {Document} from 'mongoose'
export interface IMessage extends Document {
    id?: string;
    conversationId?: string;
    senderId: string;
    receiverId: string;
    message: string;
    type: number;
    attachmentUrl?: string | null;
    delivered?: boolean;
    sent: string;
    createdAt?: string;
    updatedAt?: string;
    profanity: {
        censor: string,
        offensive: boolean
    },
    sender?: IUser,
    receiver?: IUser
}

export interface IMessageRepository {
    save(message: IMessage): Promise<string | IMessage>;
    update(message: IMessage): Promise<boolean>
    findById(id: string): Promise<null | IMessage>;
    conversationsForUserId(userId: string, limit: number): Promise<[]>
    conversationByConversationId(conversationId: string, limit: number): Promise<any>
    messagesForConversationId(conversationId: string, limit: number): Promise<[]>
    conversationIdForSenderAndReceiver(senderId: string, receiverId: string): Promise<string>;
}
