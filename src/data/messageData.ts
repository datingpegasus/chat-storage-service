export const messageData = {
    id: '123dsa',
    conversationId: '32131dasd',
    receiverId: '21313das',
    senderId: '321313',
    message: 'this is test message',
    type: 1,
    attachmentUrl: 'http://www.google.com',
    sent: '12/03/1991 12:43:00',
    createdAt: '12/03/1991 12:43:00',
    updatedAt: '12/03/1991 12:43:00',
    delivered: true,
    profanity: {
        offensive: false,
        censor: 'this is test message'
    },
    sender: {
        id: '12345',
        email: 'test@test.com',
        firstName: 'test',
        lastName: 'test',
        verified: false,
        createdAt: '12/03/1991 12:43:00',
        updatedAt: '12/03/1991 12:43:00'
    },
    receiver: {
        id: '12345',
        email: 'test2@test.com',
        firstName: 'test2',
        lastName: 'test2',
        verified: false,
        createdAt: '12/03/1991 12:43:00',
        updatedAt: '12/03/1991 12:43:00'
    }
};

export const kafkaMessage = {
    key: new Buffer('1'),
    value: new Buffer(JSON.stringify(messageData)),
    timestamp: '123456',
    size: 1234,
    attributes: 123,
    offset: '1'
};
