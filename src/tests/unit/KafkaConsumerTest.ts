import * as sinon from 'sinon'
import ChatStorageService from '../../services/ChatStorageService';
import KafkaConsumer from '../../kafka/KafkaConsumer';
import {IKafkaConsumer, KafkaSubscribeTopics} from '../../models'
import {Consumer} from 'kafkajs';
import {expect} from 'chai'
import {kafka} from '../../modules/kafka';
import {kafkaMessage, messageData} from '../../data/messageData';

let kafkaInstance: sinon.SinonStubbedInstance<Consumer>;
let chatStorageService: sinon.SinonStubbedInstance<ChatStorageService>;
let kafkaConsumer: IKafkaConsumer;
describe('Unit Testing For Kafka Consumer', () => {
    beforeEach(() => {
        kafkaInstance = sinon.stub(kafka.consumer({groupId: 'test'}));
        chatStorageService = sinon.createStubInstance(ChatStorageService);
        kafkaConsumer = new KafkaConsumer(kafkaInstance, chatStorageService)
    });
    describe('Testing sentMessages method', () => {
        it('it should not commit offset if chatStorageService returns false', async () => {
            // @ts-ignore
            chatStorageService.sentMessage.withArgs(messageData).resolves(false);
            const response = await kafkaConsumer.processKafkaTopic(KafkaSubscribeTopics.sentMessages, kafkaMessage, 1);
            expect(response).to.be.undefined;
            // @ts-ignore
            expect(chatStorageService.sentMessage.withArgs(messageData).calledOnce).to.be.true;
            expect(kafkaInstance.commitOffsets.withArgs([{
                topic: KafkaSubscribeTopics.sentMessages,
                partition: 1,
                offset: '1'
            }]).notCalled).to.be.true;
        });
        it('it should commit offset if chatStorageService returns true', async () => {
            // @ts-ignore
            chatStorageService.sentMessage.withArgs(messageData).resolves(true);
            const response = await kafkaConsumer.processKafkaTopic(KafkaSubscribeTopics.sentMessages, kafkaMessage, 1);
            expect(response).to.be.undefined
            // @ts-ignore
            expect(chatStorageService.sentMessage.withArgs(messageData).calledOnce).to.be.true
            expect(kafkaInstance.commitOffsets.withArgs([{
                topic: KafkaSubscribeTopics.sentMessages,
                partition: 1,
                offset: '1'
            }]).calledOnce).to.be.true;
        });
    });
    describe('Testing deliveredMessage', () => {
        it('it should not commit offset if chatStorageService for delivered message returns false', async () => {
            // @ts-ignore
            chatStorageService.deliveredMessage.withArgs(messageData).resolves(false);
            const response = await kafkaConsumer.processKafkaTopic(KafkaSubscribeTopics.deliveredMessages, kafkaMessage, 1)
            expect(response).to.be.undefined
            // @ts-ignore
            expect(chatStorageService.deliveredMessage.withArgs(messageData).calledOnce).to.be.true
            expect(kafkaInstance.commitOffsets.withArgs([{
                topic: KafkaSubscribeTopics.deliveredMessages,
                partition: 1,
                offset: '1'
            }]).notCalled).to.be.true;
        });
        it('it should commit offset if chatStorageService returns true', async () => {
            // @ts-ignore
            chatStorageService.deliveredMessage.withArgs(messageData).resolves(true);
            const response = await kafkaConsumer.processKafkaTopic(KafkaSubscribeTopics.deliveredMessages, kafkaMessage, 1)
            expect(response).to.be.undefined
            // @ts-ignore
            expect(chatStorageService.deliveredMessage.withArgs(messageData).calledOnce).to.be.true
            expect(kafkaInstance.commitOffsets.withArgs([{
                topic: KafkaSubscribeTopics.deliveredMessages,
                partition: 1,
                offset: '1'
            }]).calledOnce).to.be.true;
        });
    })
});
