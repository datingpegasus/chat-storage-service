import ChatStorageService from '../../services/ChatStorageService';
import MessageRepository from '../../message/MessageRepository';
import {IKafkaProducer, IMessageRepository, IChatStorageService, IRedis} from '../../models'
import * as sinon from 'sinon'
import KafkaProducer from '../../kafka/KafkaProducer';
import {messageData} from '../../data/messageData';
import {expect} from 'chai'
import Redis from '../../services/Redis';

let chatStorageService: IChatStorageService;
let chatRepository: sinon.SinonStubbedInstance<IMessageRepository>;
let kafkaProducer: sinon.SinonStubbedInstance<IKafkaProducer>;
// @ts-ignore
let redis = sinon.SinonStubbedInstance < IRedis >
    describe('Unit Test For Chat Storage Service', () => {
        beforeEach(async () => {
            chatRepository = sinon.createStubInstance(MessageRepository)
            kafkaProducer = sinon.createStubInstance(KafkaProducer)
            // @ts-ignore
            redis = sinon.createStubInstance(Redis);
            // @ts-ignore
            chatStorageService = new ChatStorageService(chatRepository, kafkaProducer, redis);
        });
        describe('Testing sentMessage method', () => {
            it('should not push to kafka topic if message is not saved', async () => {
                // @ts-ignore
                chatRepository.save.withArgs(messageData).resolves('error');
                // @ts-ignore
                const sentMessage = await chatStorageService.sentMessage(messageData);
                expect(sentMessage).to.be.false;
                // @ts-ignore
                expect(chatRepository.save.withArgs(messageData).calledOnce).to.be.true;
                // @ts-ignore
                expect(kafkaProducer.pushToSavedMessage.withArgs(messageData).notCalled).to.be.true;
            });
            /*it('should return true and emit if user is saved in database', async () => {
                // @ts-ignore
                chatRepository.save.withArgs(messageData).resolves({});
                // @ts-ignore
                kafkaProducer.pushToSavedMessage.withArgs(messageData).resolves(true)
                // @ts-ignore
                const sentMessage = await chatStorageService.sentMessage(messageData);
                expect(sentMessage).to.be.true;
                // @ts-ignore
                expect(chatRepository.save.withArgs(messageData).calledOnce).to.be.true;
                // @ts-ignore
                expect(kafkaProducer.pushToSavedMessage.withArgs(messageData).calledOnce).to.be.true;
            });*/
        })
        describe('Testing delivered message', () => {
            it('should not push to kafka topic if message is not saved', async () => {
                // @ts-ignore
                chatRepository.update.withArgs(messageData).resolves(false);
                // @ts-ignore
                const sentMessage = await chatStorageService.deliveredMessage(messageData);
                expect(sentMessage).to.be.false;
                // @ts-ignore
                expect(chatRepository.update.withArgs(messageData).calledOnce).to.be.true;
                // @ts-ignore
                expect(kafkaProducer.pushToDeliveredMessageSaved.withArgs(messageData).notCalled).to.be.true;
            });
            it('should return true and emit if user is saved in database', async () => {
                // @ts-ignore
                chatRepository.update.withArgs(messageData).resolves(true);
                // @ts-ignore
                kafkaProducer.pushToDeliveredMessageSaved.withArgs(messageData).resolves(true)
                // @ts-ignore
                const sentMessage = await chatStorageService.deliveredMessage(messageData);
                expect(sentMessage).to.be.true;
                // @ts-ignore
                expect(chatRepository.update.withArgs(messageData).calledOnce).to.be.true;
                // @ts-ignore
                expect(kafkaProducer.pushToDeliveredMessageSaved.withArgs(messageData).calledOnce).to.be.true;
            });
        })
    });
