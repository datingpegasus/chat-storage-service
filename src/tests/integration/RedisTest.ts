import {expect} from 'chai'
import Redis from '../../services/Redis';
import {IRedis, RedisKeys} from '../../models';
let redis: IRedis;
describe('Integration Tests For Message Repository', () => {
	before(async () => {
	    await Redis.factory().connectToRedis();
    	redis = Redis.factory();
	});
	it('Should return empty Array if there is h set in redis ', async () => {
		const response = await redis.hGet(RedisKeys.userConversations, '1')
		// @ts-ignore;
		expect(response).to.be.array;
    });
	it('Should return array of data for existing user in redis', async () => {
		const set = await redis.hSet('user-conversations', '1', JSON.stringify([{id: 1}]));
		expect(set).to.be.true
		// @ts-ignore
		const response: [] = await redis.hGet(RedisKeys.userConversations, '1');
		// @ts-ignore
		expect(response).to.be.array;
		expect(response.length).to.be.eq(1)

	});
});
