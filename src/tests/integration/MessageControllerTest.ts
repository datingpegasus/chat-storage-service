import * as chai from 'chai'
import * as supertest from 'supertest'
import app from '../../../app';
import MessageRepository from '../../message/MessageRepository';
import {messageData} from '../../data/messageData';
describe('Testing Message Controller Api', () => {
   it('should return null if message is not found', () => {
      supertest(app).get('/api/message/1')
          .expect(200)
          .then((response) => {
             chai.expect(response.body.message).to.be.eq('null')
          })
   });
   it('should return message object is message exists', async () => {
      const messageRepository = new MessageRepository();
      // @ts-ignore
      const savedMessage = await messageRepository.save(messageData);
      supertest(app).get('/api/message/' + savedMessage.id)
          .set('Accept', 'application/json')
          .expect(200)
          .then((response) => {
             chai.expect(response.body.message.id).to.be.eq(savedMessage.id)
          })
   });
});
