import * as mongoose from 'mongoose';
import config from '../../../config/environment';
import Message from '../../message/Message';
import {IMessage} from '../../models'
import {expect} from 'chai'
import {messageData} from '../../data/messageData';
import * as sinon from 'sinon'
describe('Testing Message Model', () => {
    before(async () => {
        await mongoose.connect(config.mongo.uri, config.mongo.options);
    });
    after(async () => {
        await Message.deleteMany({});
        await mongoose.disconnect();
    });
    it('should throw validation error', async () => {
        const message = new Message();
        try {
            await message.validate()
        } catch (e) {
            expect(e.name).to.be.eq('ValidationError')
        }
    })
    it('should save a user', async () => {
        const message: IMessage = new Message(messageData)
        const spy = sinon.spy(message, 'save');
        await message.save();
        expect(spy.calledOnce).to.be.true
        expect(message.receiverId).to.be.eq(messageData.receiverId)
    });
    it('should have virtual id same as local id', async () => {
        const message: IMessage = new Message(messageData)
        await message.save();
        const messageJson = message.toJSON()
        expect(messageJson.id).to.be.eq(messageJson.id)
    })
    it('should update user', async () => {
        const localData = JSON.parse(JSON.stringify(messageData));
        localData.delivered = false;
        const message: IMessage = new Message(localData);
        await message.save();
        expect(message.delivered).to.be.false;
        message.delivered = true;
        await message.save()
        expect(message.delivered).to.be.true;
    });
    it('should update user using save method', async () => {
        const localData = JSON.parse(JSON.stringify(messageData));
        localData.delivered = false;
        const message: IMessage = new Message(localData);
        await message.save();
        expect(message.delivered).to.be.false;
        await Message.updateOne({_id: message.id}, {delivered: true});
        const fetchedMessage = await Message.findById(message.id)
        expect(fetchedMessage.delivered).to.be.true;
    })
});
