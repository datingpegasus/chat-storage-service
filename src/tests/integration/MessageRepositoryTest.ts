import * as mongoose from 'mongoose';
import config from '../../../config/environment';
import Message from '../../message/Message';
import {IMessage} from '../../models'
import MessageRepository from '../../message/MessageRepository';
import {expect} from 'chai'
import {messageData} from '../../data/messageData';
describe('Integration Tests For Message Repository', () => {
    before(async () => {
        await mongoose.connect(config.mongo.uri, config.mongo.options);
    });
    after(async () => {
        //await Message.deleteMany({});
        await mongoose.disconnect();
    });
    it('should throw validation error and return message', async () => {
       const messageRepository = new MessageRepository();
       // @ts-ignore
       const savedUser = await messageRepository.save({});
       expect(savedUser).to.be.string(savedUser);
   })
    it('should save user and return user object', async () => {
        const messageRepository = new MessageRepository();
        // @ts-ignore
        const savedUser: IMessage = await messageRepository.save(messageData);
        expect(savedUser).to.be.instanceOf(Message)
        expect(savedUser.receiverId).to.be.equal(messageData.receiverId)
    })
    it('should update user message data for delivered status', async () => {
        const messageRepository = new MessageRepository();
        const localData: IMessage = JSON.parse(JSON.stringify(messageData))
        localData.delivered = false;
        // @ts-ignore
        const savedMessage: IMessage = await messageRepository.save(localData);
        const savedMessageJson: IMessage = savedMessage.toJSON();
        savedMessageJson.delivered = true;
        expect(await messageRepository.update(savedMessageJson)).to.be.true;
    })
    it('should return null if there is no message in database', async () => {
        const messageRepository = new MessageRepository();
        expect(await messageRepository.findById('1')).to.be.null
    })
    it('should return message record if message is found in database', async () => {
        const messageRepository = new MessageRepository();
        const message = new Message(messageData);
        const savedMessage = await message.save();
        expect(await messageRepository.findById(savedMessage.id)).to.be.instanceOf(Message)
    })
    it('should return empty if there is no conversations for user', async () => {
        const messageRepository = new MessageRepository();
        const message = new Message(messageData);
        const savedMessage = await message.save();
        const conversations = await messageRepository.conversationsForUserId('1', 1);

        expect(conversations).to.be.an('array');
        expect(conversations.length).to.be.eq(0)
    });
    it('should return one conversation where sender id is correct', async () => {
        const messageRepository = new MessageRepository();
        const message = new Message(messageData);
        const savedMessage = await message.save();
        const conversations = await messageRepository.conversationsForUserId(messageData.senderId, 1);
        expect(conversations).to.be.an('array')
        expect(conversations.length).to.be.eq(1)
    });
    it('should return one conversation where reciever id is correct', async () => {
        const messageRepository = new MessageRepository();
        const message = new Message(messageData);
        const savedMessage = await message.save();
        const conversations = await messageRepository.conversationsForUserId(messageData.receiverId, 1);
        expect(conversations).to.be.an('array');
        expect(conversations.length).to.be.eq(1)
    });
    it('should return last conversation object by conversationId', async () => {
        const messageRepository = new MessageRepository();
        const message = new Message(messageData);
        const savedMessage = await message.save();
        const conversations = await messageRepository.conversationByConversationId(messageData.conversationId, 1);
        expect(conversations).to.be.an('array');
        expect(conversations.length).to.be.eq(1)
    });
    it('Should return message for conversation id', async () => {
        const messageRepository = new MessageRepository();
        const message = new Message(messageData);
        const savedMessage = await message.save();
        const conversations = await messageRepository.messagesForConversationId(messageData.conversationId, 1);
        expect(conversations).to.be.an('array')
        expect(conversations.length).to.be.eq(1)
    });
    it('Should return conversation id for sender nad receiver', async () => {
        const messageRepository = new MessageRepository();
        const message = new Message(messageData);
        const savedMessage = await message.save();
        const conversations = await messageRepository
            .conversationIdForSenderAndReceiver(messageData.senderId, messageData.receiverId);
        expect(conversations).to.be.eq(messageData.conversationId)
    });
    it('Should return null if users are not connected', async () => {
        const messageRepository = new MessageRepository();
        const message = new Message(messageData);
        const savedMessage = await message.save();
        const conversations = await messageRepository
            .conversationIdForSenderAndReceiver('321321', messageData.receiverId);
        expect(conversations).to.be.eq(null)
    });
    it('Should return conversation id if receiver is the sender', async () => {
        const messageRepository = new MessageRepository();
        const message = new Message(messageData);
        const savedMessage = await message.save();
        const conversations = await messageRepository
            .conversationIdForSenderAndReceiver(messageData.receiverId, messageData.senderId);
        expect(conversations).to.be.eq(messageData.conversationId)
    })
});
