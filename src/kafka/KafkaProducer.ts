import {IMessage, IKafkaProducer, KafkaProducerTopics} from '../models';
import * as kafka from 'kafkajs'

export default class KafkaProducer implements IKafkaProducer {
    public  producer: kafka.Producer;
    public constructor(producer: kafka.Producer) {
        this.producer = producer;
    }
    public async pushToDeliveredMessageSaved(message: IMessage): Promise<boolean> {
        return this.pushToKafkaTopic(message, KafkaProducerTopics.deliveredMessagesSaved);
    }

    public async pushToSavedMessage(message: IMessage): Promise<boolean> {
        return this.pushToKafkaTopic(message, KafkaProducerTopics.savedMessages);
    }

    public async pushUserConversations(conversations: any): Promise<boolean> {
        return this.pushToKafkaTopic(conversations, KafkaProducerTopics.userConversations);
    }

    public async pushUserConversationMessages(conversationMessages: IMessage[], userId: string): Promise<boolean> {
        return this.pushToKafkaTopic({messages: conversationMessages, userId}, KafkaProducerTopics.userConversationMessages)
    }

    public async pushToKafkaTopic(data: any, topic: string): Promise<boolean> {
        const sent = await this.producer.send({
            topic,
            messages: [{value: JSON.stringify(data)}]
        })
        return (sent[0].errorCode === 0);
    }
}
