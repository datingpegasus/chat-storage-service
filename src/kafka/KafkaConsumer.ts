import * as kafka from 'kafkajs'
import Logger from '../services/Logger';
import {IChatStorageService, IKafkaConsumer, KafkaSubscribeTopics} from '../models';

export default class KafkaConsumer implements IKafkaConsumer {
    private chatStorageService: IChatStorageService;
    private kafkaConsumer: kafka.Consumer;

    public constructor(kafkaConsumer: kafka.Consumer, chatStorageService: IChatStorageService) {
        this.chatStorageService = chatStorageService;
        this.kafkaConsumer = kafkaConsumer;
    }
    public async processKafkaTopic(topic: string, kafkaMessage: kafka.KafkaMessage, partition: number): Promise<void> {
        try {
            const kafkaParsedMessage = JSON.parse(kafkaMessage.value.toString());
            let saved = false;
            if (topic === KafkaSubscribeTopics.sentMessages) {
                saved = await this.chatStorageService.sentMessage(kafkaParsedMessage);
            } else if (topic === KafkaSubscribeTopics.messageDelivered) {
                saved = await this.chatStorageService.deliveredMessage(kafkaParsedMessage);
            } else if (topic === KafkaSubscribeTopics.userConnectedToSocket) {
                saved = await this.chatStorageService.userConnectedToSocket(kafkaParsedMessage['userId']);
            } else if (topic === KafkaSubscribeTopics.userAuthenticated) {
                saved = await this.chatStorageService.userAuthenticated(kafkaParsedMessage);
            } else if (topic === KafkaSubscribeTopics.userConnectedToConversation) {
                saved = await this.chatStorageService.userConnectedToConversation(kafkaParsedMessage);
            } else {
                Logger.warn({kafka: `Wrong kafka topic name ${topic}`});
            }
            if (saved) {
                await this.commitOffset(topic, partition, kafkaMessage.offset);
            }
        } catch (e) {
            Logger.error(e);
        }
    }

    public async subscribeToTopics(): Promise<void> {
        for (const [topicKey, topicValue] of Object.entries(KafkaSubscribeTopics)) {
            await this.kafkaConsumer.subscribe({topic: topicValue})
        }
    }

    public async commitOffset(topicName: string, partition: number, offset: string): Promise<boolean> {
        try {
            await this.kafkaConsumer.commitOffsets([{
                topic: topicName,
                partition,
                offset
            }]);
            return true;
        } catch (e) {
            Logger.error(e);
            return false;
        }
    }
}
