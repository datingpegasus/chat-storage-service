import {IMessage, IMessageRepository} from '../models';
import Message from './Message'
import Logger from '../services/Logger';
export default class MessageRepository implements IMessageRepository {
    public async save(message: IMessage): Promise<string | IMessage> {
        try {
            const savedMessage = new Message(message);
            await savedMessage.validate();
            return await savedMessage.save();
        } catch (e) {
            return e.message
        }
    }

    public async update(message: IMessage): Promise<boolean> {
        try {
            await Message.updateOne({_id: message.id}, message)
            return true;
        } catch (e) {
            Logger.error(e);
            return false;
        }
    }

    public async findById(id: string): Promise<null | IMessage> {
        try {
            return await Message.findById(id)
        } catch (e) {
            return null;
        }
    }

    public async conversationsForUserId(userId: string, limit: number): Promise<any> {
        try {
            return await Message.aggregate([
                {$match: {$or: [{senderId: userId}, {receiverId: userId}]}},
                {$group: {_id: '$conversationId'}},
                {$sort: {createdAt: -1}}
            ])
        } catch (e) {
            Logger.error(e);
            return []
        }
    }

    public async conversationByConversationId(conversationId: string, limit: number): Promise<any> {
        try {
            return await Message.aggregate([
                {$match: {conversationId}},
                {$sort: {createdAt: -1}}, {$limit: limit}])
        } catch (e) {
            Logger.error(e);
            return []
        }
    }

    public async messagesForConversationId(conversationId: string, limit: number): Promise<any> {
        try {
            return await Message.aggregate([
                {$match: {conversationId}},
                {$sort: {createdAt: 1}}, {$limit: limit}])
        } catch (e) {
            Logger.error(e);
            return []
        }
    }
    public async conversationIdForSenderAndReceiver(senderId: string, receiverId: string): Promise<any> {
        try {
            let conversation = await Message.findOne({senderId, receiverId});
            if (conversation !== null) {
                return conversation.conversationId
            }
            conversation =  await Message.findOne({senderId: receiverId, receiverId: senderId});
            if (conversation !== null) {
                return conversation.conversationId
            }
            return null;
        } catch (e) {
            Logger.error(e);
            return null
        }
    }
}
