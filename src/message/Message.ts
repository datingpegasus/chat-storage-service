import {Schema, model} from 'mongoose';
import { v4 as uuid } from 'uuid';
import {IMessage} from '../models';

const MessageSchema = new Schema({
    conversationId: {type: String, required: true, index: true, default: uuid()},
    senderId: {type: String, required: true},
    receiverId: {type: String, required: true},
    message: {type: String, required: false},
    type: {type: Number, required: true},
    attachmentUrl: {type: String, required: false},
    delivered: {type: Boolean, required: true, default: false},
    sent: {type: String, required: true},
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now},
    profanity: {
        censor: {type: String, required: true},
        offensive: {type: Boolean, required: true}
    },
    sender: {
        id: {type: String, required: true },
        email: {type: String, required: true},
        firstName: {type: String, required: true},
        lastName: {type: String, required: true},
        verified: {type: Boolean, required: true},
        createdAt: {type: Date, required: true},
        updatedAt: {type: Date, required: true}
    },
    receiver: {
        id: {type: String },
        email: {type: String, required: true},
        firstName: {type: String, required: true},
        lastName: {type: String, required: true},
        verified: {type: Boolean, required: true},
        createdAt: {type: Date, required: true},
        updatedAt: {type: Date, required: true}
    }
});

MessageSchema.set('toJSON', {
    transform (doc: any, ret: any) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
export default model<IMessage>('Message', MessageSchema)
