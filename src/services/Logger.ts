import logger from '../modules/logger'
export default class Logger {
    public static info(data: object): void {
        console.log(data);
        logger.emit('INFO', {info: data})
    }

    public static warn(data: object): void {
        console.log(data);
        logger.emit('WARNING', {data});
    }

    public static error(data: object): void {
        console.log(data);
        logger.emit('ERROR', {data})
    }

    public static userConnected(userId: string): void {
        Logger.info({connection: `User with ${userId} Connected To Socket`, userId})
    }

    public static userDisconnected(userId: string): void {
        Logger.info({disconnect: `User with ${userId} Disconnected To Socket`, userId})
    }
}
