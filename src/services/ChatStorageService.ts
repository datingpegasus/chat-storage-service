import {RedisKeys, IMessage, IChatStorageService, IMessageRepository,
    IRedis, IUser, IKafkaProducer} from '../models';

export default class ChatStorageService implements IChatStorageService {
    private messageRepository: IMessageRepository;
    private kafkaProducer: IKafkaProducer;
    private readonly redis: IRedis;

    constructor(messageRepository: IMessageRepository, kafkaProducer: IKafkaProducer, redis: IRedis) {
        this.messageRepository = messageRepository;
        this.kafkaProducer = kafkaProducer;
        this.redis = redis;
    }

    public async sentMessage(message: IMessage): Promise<boolean> {
        try {
            if (typeof message.sender === 'undefined') {
                const user = await this.redis.hGet(RedisKeys.authenticatedUserChatStorage, message.senderId);
                if (!Array.isArray(user)) message['sender'] = user
            }
            if (typeof message.receiver === 'undefined') {
                const user = await this.redis.hGet(RedisKeys.authenticatedUserChatStorage, message.receiverId);
                if (!Array.isArray(user)) message['receiver'] = user
            }
            if (typeof message.conversationId === 'undefined') {
                const conversationId = await this.messageRepository
                    .conversationIdForSenderAndReceiver(message.senderId, message.receiverId)
                if (conversationId !== null) {
                    message['conversationId'] = conversationId;
                }
            }

            const messageSaved = await this.messageRepository.save(message);
            if (typeof messageSaved === 'string') return false;
            return await this.kafkaProducer.pushToSavedMessage(messageSaved);
        } catch (e) {
            return false;
        }
    }

    public async deliveredMessage(message: IMessage): Promise<boolean> {
        try {
            const messageSaved = await this.messageRepository.update(message);
            return (messageSaved) ? await this.kafkaProducer.pushToDeliveredMessageSaved(message) : false;
        } catch (e) {
            return false;
        }
    }

    public async userConnectedToSocket(userId: string): Promise<boolean> {
        try {
            const redisConversations = await this.redis.hGet(RedisKeys.userConversations, userId);
            if (redisConversations.length !== 0) {

                return await this.kafkaProducer.pushUserConversations(
                    {conversations: redisConversations, userId})
            }
            const conversationsForUser = await this.messageRepository.conversationsForUserId(userId, 5)
            if (conversationsForUser.length === 0) {
                return await this.kafkaProducer.pushUserConversations({conversations: conversationsForUser, userId})
            }
            const conversations = [];
            for (const userConversation of conversationsForUser) {
                const conversation = await this.messageRepository.conversationByConversationId(
                    userConversation['_id'], 1);
                if (conversation.length > 0) conversations.push(conversation[0])
            }
            await this.redis.hSet(RedisKeys.userConversations, userId, JSON.stringify(conversations));
            return await this.kafkaProducer.pushUserConversations({conversations, userId})
        } catch (e) {
            console.log(e.toString());
            return false
        }
    }

    public async userAuthenticated(user: IUser): Promise<boolean> {
        try {
            return await this.redis.hSet(RedisKeys.authenticatedUserChatStorage, user.id, JSON.stringify(user));
        } catch (e) {
            return false;
        }
    }

    public async userConnectedToConversation(conversation: {conversationId: string, userId: string}): Promise<boolean> {
        try {
            const conversations = await this.messageRepository.messagesForConversationId(conversation.conversationId, 10);
            return await this.kafkaProducer.pushUserConversationMessages(conversations, conversation.userId);
        } catch (e) {
            return false;
        }
    }
}
