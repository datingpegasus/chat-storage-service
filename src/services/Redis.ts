import * as redis from 'redis'
import config, {IConfig} from '../../config/environment'
import {IRedis} from '../models';

export default class Redis implements IRedis {
    private static instance: IRedis;
    private readonly config: IConfig;
    private client: redis.RedisClient;

    public static factory(): IRedis {
        if (!Redis.instance) {
            Redis.instance = new Redis()
        }
        return Redis.instance;
    }

    public connectToRedis(): Promise<redis.RedisClient> {
        return new Promise<redis.RedisClient>((resolve, reject) => {
            this.client = redis.createClient(config.redisPort, config.redisUrl);
            this.client.on('ready', (connected: any) => {
                resolve(this.client);
            });
            this.client.on('error', (error: any) => {
                reject(error);
            });
        });
    }

    public subscribe(client: redis.RedisClient): Promise<redis.RedisClient> {
        return new Promise<redis.RedisClient>((resolve, reject) => {
            client.on('subscribe', (channel: string, count: number) => {
                return resolve(client);
            })
            return resolve(client);
        });
    }

    public async exists(key: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.client.exists(key, (error: Error, value: number) => {
                if (error) return reject(new Error(error.toString()))
                return resolve((value === 1))
            })
        });
    }

    public async get(key: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.client.get(key, (error: Error, value: any) => {
                if (error) return reject(new Error(error.toString()))
                return resolve(value)
            })
        })
    }

    public async set(key: string, value: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.client.set(key, value, (error: Error, res: string) => {
                if (error) return reject(new Error(error.toString()));
                return resolve(res === 'OK')
            })
        })
    }

    public async hGetAll(key: string): Promise<object> {
        return new Promise<object>((resolve, reject) => {
            this.client.hgetall(key, (error: Error | null, value: object) => {
                if (error) return reject(new Error(error.toString()));
                return resolve(value)
            });
        });
    }
    //TODO fis hGetMethod
    // @ts-ignore
    public async hGet(key: string, indexKey: string): Promise<any> {
        // @ts-ignore
        return new Promise<array>((resolve, reject) => {
            this.client.hget(key, indexKey, (error: Error | null, value: string) => {
                if (error) return reject(new Error(error.toString()));
                if (value === null) return resolve([]);
                return resolve(JSON.parse(value));
            });
        });
    }
    public async hSet(key: string, hKey: string, value: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.client.hset(key, hKey, value, (error: Error | null, numberInRedis: number) => {
                if (error) return reject(new Error(error.toString()));
                return resolve(true);
            });
        });
    }
}
