import * as controller from './message.controller'
import { Router } from 'express'

const router = Router()

router.get('/:id', controller.getMessageById);

export default router
