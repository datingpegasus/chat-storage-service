import { apiMethod } from '../helpers';
import MessageRepository from '../../message/MessageRepository';

export const getMessageById = apiMethod<{ message: string }>(async (req) => {
    let message = null;
    if (req.params.id) {
        const messageRepository = new MessageRepository();
        message = await messageRepository.findById(req.params.id)
    }
    return {
        data: { message: JSON.stringify(message) },
        status: 200,
    }
});
