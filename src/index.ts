import * as mongoose from 'mongoose';
import config from '../config/environment';
import {kafka} from './modules/kafka';
import KafkaProducer from './kafka/KafkaProducer';
import ChatStorageService from './services/ChatStorageService';
import MessageRepository from './message/MessageRepository';
import KafkaConsumer from './kafka/KafkaConsumer';
import Redis from './services/Redis'
import Logger from './services/Logger';
export default async function setup() {
    try {
        // Connect to database
        await mongoose.connect(config.mongo.uri, config.mongo.options);
        const producer = kafka.producer();
        await producer.connect();
        const redis = Redis.factory();
        const redisClient = await redis.connectToRedis();
        const kafkaProducer = new KafkaProducer(producer);
        const chatStorageService = new ChatStorageService(new MessageRepository(), kafkaProducer, redis)
        const consumer = kafka.consumer({groupId: 'ChatStorageGroup'});
        const kafkaConsumer = new KafkaConsumer(consumer, chatStorageService);
        await kafkaConsumer.subscribeToTopics();
        await consumer.run({
            autoCommit: false,
            eachMessage: async ({topic, partition, message}) => {
                await kafkaConsumer.processKafkaTopic(topic, message, partition);
            }
        })
    } catch (e) {
        Logger.error(e)
    }
}
