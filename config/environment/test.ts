// Test specific configuration
// ===========================
export default {
  allowedOrigins: [
    '*'
  ],
  mongo: {
    uri: 'mongodb://chat_storage_service_mongo/chat-test'
  },
};
