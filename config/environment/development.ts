'use strict'

// Development specific configuration
// ==================================
export default {
  allowedOrigins: [
    'http://localhost:9000',
  ],
  mongo: {
    uri: 'mongodb://chat_storage_service_mongo/chat'
  },
  logger: {
    ipAddress: '192.168.0.15',
    serviceName: 'chat-storage',
    port: 24224
  },

  kafka: {
    clientId: 'ChatStorage',
    brokers: ['192.168.0.15:9092'],
    groupId: 'ChatStorage'
  }
};
